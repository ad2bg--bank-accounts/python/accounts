#### Disclaimer:
This is a nice little task I got from a friend.  
It is just as an exercise and not to be viewed as a guideline / best practice / tutorial, etc...  
Surely it has room for improvement. 

# To run:
- Make sure you have installed Python 3.8 and pip.
- As usual:  
`mkdir my_task && cd my_task`  
`git clone https://gitlab.com/ad2bg--bank-accounts/python/accounts .`  
`sudo apt-get install python3-venv` (if module venv is not already installed)   
`python3 -m venv venv`  
`source venv/bin/activate`  
`pip install -r ./requirements.dev.txt`  

- From the project folder (my_task) run:  `python ./src/script.py ./input.csv`

# To run the unit tests:
- From the project folder (my_task) run:  `python -m unittest discover -s ./src`  


# Regarding the Requirements

- The task is to be implemented in PHP,
  however I take the liberty of implementing it in Python.
- The relevant requirements remain unchanged.
- Regarding the extensibility requirement,
  I foresee upgrading this CLI app possibly to a GUI app, or
  a web-app possibly with a framework like Django.

- The task does not mention anything about data validation,
  but it doesn't say that the data will always be valid,
  so since we're talking money here,
  I again take the liberty of implementing proper validation.
- Nothing is mentioned about console output in case of invalid data,
  so again, I take the liberty of implementing it the way
  I deem it makes sense in the real world.
  In real life, if I encounter invalid input and we are dealing with
  financial data, I'd just consider the whole file corrupt and just
  continue displaying whether each line is valid,
  or in case it isn't, then what errors are there.
  So this is what I'm implementing here.
- Finally, nothing is explicitly mentioned about whether or not a person
  (whether Natural or Legal) can have multiple accounts.
  If that would be possible, then in real life, a Natural person
  would possibly create multiple own accounts in order to benefit from
  the given cash_out commission fees calculation rules,
  **provided that there are no other related expenses**.
  However, since in the given input file we are not given any information
  about which of the person's possibly multiple accounts the operation relates to,
  we now have two implementation options:   
  a) - in the sake of extensibility,
  we could implement the code in such a way,
  that as long as we only have a singe account per person,
  that single account is automatically used in the commission fees calculation,
  and if/when in the future, multiple accounts become a part of the business logic,
  we will pass the appropriate account as an extra parameter (column) in the given csv file.
  
  b) - in the sake of keeping the code clear and concise,
  we could refrain from implementing anything supporting multiple accounts for now,
  and only if/when in the future, multiple accounts become a part of the business logic,
  go ahead and refactor the existing code to add such support.
  So, obviously this option would require a steeper implementation curve in the future.
  
  In real life we would know or ask what the business logic is,
  and the likelihood of having a more complex business logic.

  Now the requirement
  > - your system must be extensible:
  >   - adding new functionality or changing existing one should not require rewriting
  >    the system itself or it's core parts;

  really concerns me, as it may be subjective as to where the line between
  refactoring and rewriting lies.
  
  So, I'm taking option a).
