import unittest
from datetime import date

from models import Constants, PersonFactory, Storage, Account, Operation, Currency

dates = [
    date(2020, 5, 1),  # Fri
    date(2020, 5, 2),  # Sat
    date(2020, 5, 3),  # Sun
    date(2020, 5, 4),  # Mon
    date(2020, 5, 10),  # Sun
]
cash_in = Constants.OPERATION_TYPE_CASH_IN
cash_out = Constants.OPERATION_TYPE_CASH_OUT
eur = Constants.CURRENCY_EUR
usd = Constants.CURRENCY_USD
jpy = Constants.CURRENCY_JPY
rate_eur = Constants.CURRENCY_EUR_RATE
rate_usd = Constants.CURRENCY_USD_RATE
rate_jpy = Constants.CURRENCY_JPY_RATE
base_currency_token = Constants.BASE_CURRENCY_TOKEN
base_rate = Constants.BASE_CURRENCY_RATE
customer_natural = Constants.PERSON_TYPE_NATURAL
customer_legal = Constants.PERSON_TYPE_LEGAL


class TestStorage(unittest.TestCase):
    def test_init(self):
        Storage()

    def test_customer_get_or_create(self):
        storage = Storage()
        customer1 = storage.customer_get_or_create(1, customer_natural)
        self.assertEqual(len(storage.customers), 1)
        self.assertEqual(customer1.id, 1)
        self.assertEqual(customer1.type, customer_natural)
        customer2 = storage.customer_get_or_create(2, customer_legal)
        self.assertEqual(len(storage.customers), 2)
        self.assertEqual(customer2.id, 2)
        self.assertEqual(customer2.type, customer_legal)
        self.assertRaises(Exception, lambda: storage.customer_get_or_create(1, customer_legal))
        self.assertEqual(storage.customer_get_or_create(1, customer_natural), customer1)


class TestPerson(unittest.TestCase):
    def test_init(self):
        PersonFactory.create(1, customer_natural)
        PersonFactory.create(1, customer_legal)
        self.assertRaises(Exception, lambda: PersonFactory.create(0, customer_natural))
        self.assertRaises(Exception, lambda: PersonFactory.create(0, customer_legal))
        self.assertRaises(Exception, lambda: PersonFactory.create(1, None))

    def test_account_get_or_create(self):
        customer = PersonFactory.create(1, customer_natural)
        self.assertRaises(Exception, lambda: Account(customer, 0))
        account1 = customer.account_get_or_create(id=1)
        self.assertTrue(isinstance(account1, Account))
        self.assertEqual(account1.customer, customer)
        account2 = Account(customer, 2)
        self.assertTrue(isinstance(account2, Account))
        self.assertEqual(account2.customer, customer)
        self.assertNotEqual(account1, account2)
        self.assertEqual(customer.account_get_or_create(id=1), account1)


class TestAccount(unittest.TestCase):
    def setUp(self):
        self.customer_natural = PersonFactory.create(1, customer_natural)
        self.customer_legal = PersonFactory.create(2, customer_legal)

    def test_init(self):
        Account(self.customer_natural, 1)
        Account(self.customer_legal, 1)
        self.assertRaises(Exception, lambda: Account(None, 0))
        self.assertRaises(Exception, lambda: Account(None, 1))
        self.assertRaises(Exception, lambda: Account(self.customer_natural, 0))
        self.assertRaises(Exception, lambda: Account(self.customer_legal, 0))

    def test_get_cash_out_operations_for_the_current_calendar_week(self):
        account = Account(self.customer_natural, 1)
        Operation(account, dates[0], cash_out, 1000, eur)
        self.assertEqual(len(account.get_cash_out_operations_for_the_current_calendar_week(dates[0])), 1)  # Fri
        Operation(account, dates[0], cash_out, 1000, eur)
        self.assertEqual(len(account.get_cash_out_operations_for_the_current_calendar_week(dates[0])), 2)  # Fri
        Operation(account, dates[1], cash_out, 1000, eur)
        self.assertEqual(len(account.get_cash_out_operations_for_the_current_calendar_week(dates[1])), 3)  # Sat
        self.assertEqual(len(account.get_cash_out_operations_for_the_current_calendar_week(dates[3])), 0)  # Mon


class TestCurrency(unittest.TestCase):
    def test_convert_to_base_currency(self):
        self.assertEqual(Currency.convert_to_base_currency(5, eur), 5 * base_rate / rate_eur)
        self.assertEqual(Currency.convert_to_base_currency(5, usd), 5 * base_rate / rate_usd)
        self.assertEqual(Currency.convert_to_base_currency(5, jpy), 5 * base_rate / rate_jpy)
        self.assertRaises(Exception, lambda: Currency.convert_to_base_currency(5, 'usd'))
        self.assertRaises(Exception, lambda: Currency.convert_to_base_currency(5, 'crap'))

    def test_convert_to_currency(self):
        self.assertEqual(Currency.convert_to_currency(5, eur), 5 * rate_eur / base_rate)
        self.assertEqual(Currency.convert_to_currency(5, usd), 5 * rate_usd / base_rate)
        self.assertEqual(Currency.convert_to_currency(5, jpy), 5 * rate_jpy / base_rate)
        self.assertRaises(Exception, lambda: Currency.convert_to_currency(5, 'usd'))
        self.assertRaises(Exception, lambda: Currency.convert_to_currency(5, 'crap'))

    def test_round_currency(self):
        self.assertEqual(Currency.round_currency(5.1, eur), '5.10')
        self.assertEqual(Currency.round_currency(5.1, usd), '5.10')
        self.assertEqual(Currency.round_currency(5.1, jpy), '6')
        self.assertEqual(Currency.round_currency(0, eur), '0.00')
        self.assertEqual(Currency.round_currency(-0.1, eur), '-0.10')
        self.assertRaises(Exception, lambda: Currency.round_currency(None, eur))
        self.assertRaises(Exception, lambda: Currency.round_currency(5, ''))
        self.assertRaises(Exception, lambda: Currency.round_currency(5, None))


class TestOperation(unittest.TestCase):
    def setUp(self):
        self.customer_natural = PersonFactory.create(1, customer_natural)
        self.customer_legal = PersonFactory.create(2, customer_legal)
        self.account1 = Account(self.customer_natural, 1)
        self.account2 = Account(self.customer_legal, 2)

    def test_init(self):
        Operation(self.account1, dates[0], cash_in, 100, eur)
        Operation(self.account2, dates[0], cash_in, 100, eur)
        Operation(self.account1, dates[0], cash_out, 100, eur)
        Operation(self.account2, dates[0], cash_out, 100, eur)
        self.assertRaises(Exception, lambda: Operation(None, dates[0], cash_in, 100, eur))
        self.assertRaises(Exception, lambda: Operation(self.account1, None, cash_in, 100, eur))
        self.assertRaises(Exception, lambda: Operation(self.account1, dates[0], None, 100, eur))
        self.assertRaises(Exception, lambda: Operation(self.account1, dates[0], cash_in, 0, eur))
        self.assertRaises(Exception, lambda: Operation(self.account1, dates[0], cash_in, 100, None))

    def test_calculate_fee_for_cash_in(self):
        self.assertEqual(Operation(self.account1, dates[0], cash_in, 100, eur).calculate_fee(), '0.03')
        self.assertEqual(Operation(self.account2, dates[0], cash_in, 100, eur).calculate_fee(), '0.03')
        self.assertRaises(Exception, lambda: Operation(self.account1, dates[0], cash_in, 0, eur).calculate_fee())

    def test_calculate_fee_for_cash_out_for_natural_person(self):
        self.assertEqual(Operation(self.account1, dates[0], cash_out, 1200, eur).calculate_fee(), '0.60')
        self.assertEqual(Operation(self.account1, dates[0], cash_out, 1000, eur).calculate_fee(), '3.00')
        self.assertEqual(Operation(self.account1, dates[1], cash_out, 1000, eur).calculate_fee(), '3.00')
        self.assertEqual(Operation(self.account1, dates[3], cash_out, 500, eur).calculate_fee(), '0.00')
        self.assertEqual(Operation(self.account1, dates[3], cash_out, 500, eur).calculate_fee(), '0.00')
        self.assertEqual(Operation(self.account1, dates[4], cash_out, 600, eur).calculate_fee(), '1.80')
        self.assertRaises(Exception, lambda: Operation(self.account1, dates[4], cash_out, 0, eur).calculate_fee())

    def test_calculate_fee_for_cash_out_for_legal_person(self):
        self.assertEqual(Operation(self.account2, dates[0], cash_out, 100, eur).calculate_fee(), '0.50')
        self.assertRaises(Exception, lambda: Operation(self.account2, dates[0], cash_out, 0, eur).calculate_fee())
