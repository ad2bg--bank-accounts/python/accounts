import unittest
from datetime import date
from decimal import Decimal, InvalidOperation

from models import Constants, PersonFactory, Account
from validation import (
    is_valid_id,
    is_valid_customer_type,
    is_valid_date,
    is_valid_operation_type,
    is_valid_operation_amount,
    is_valid_currency_token,
    to_decimal,
    ValidationErrors,
    get_validation_errors,
)

natural = Constants.PERSON_TYPE_NATURAL
legal = Constants.PERSON_TYPE_LEGAL
cash_in = Constants.OPERATION_TYPE_CASH_IN
cash_out = Constants.OPERATION_TYPE_CASH_OUT
eur = Constants.CURRENCY_EUR
usd = Constants.CURRENCY_USD
jpy = Constants.CURRENCY_JPY


class TestValidationFunctions(unittest.TestCase):
    def test_is_valid_id(self):
        self.assertFalse(is_valid_id(None))
        self.assertFalse(is_valid_id(''))
        self.assertFalse(is_valid_id('0'))
        self.assertFalse(is_valid_id(0))
        self.assertFalse(is_valid_id(-1))
        self.assertFalse(is_valid_id(1.0))
        self.assertFalse(is_valid_id('1.0'))
        self.assertFalse(is_valid_id('1,0'))
        self.assertFalse(is_valid_id(1.00000000000000001))
        self.assertFalse(is_valid_id(0.99999999999999999))
        self.assertFalse(is_valid_id(1e0))
        self.assertTrue(is_valid_id(1))
        self.assertTrue(is_valid_id(Decimal(1)))
        self.assertTrue(is_valid_id(12345678901234567890))

    def test_is_valid_customer_type(self):
        self.assertFalse(is_valid_customer_type(None))
        self.assertFalse(is_valid_customer_type(''))
        self.assertFalse(is_valid_customer_type('crap'))
        self.assertFalse(is_valid_customer_type(0))
        self.assertTrue(is_valid_customer_type(natural))
        self.assertTrue(is_valid_customer_type(legal))

    def test_is_valid_date(self):
        self.assertFalse(is_valid_date(None))
        self.assertFalse(is_valid_date(''))
        self.assertFalse(is_valid_date(0))
        self.assertFalse(is_valid_date('2019/02/28'))
        self.assertFalse(is_valid_date('2019-02-29'))
        self.assertTrue(is_valid_date(date(2020, 1, 1)))
        self.assertTrue(is_valid_date('2020-01-01'))
        self.assertTrue(is_valid_date('2020-1-1'))
        self.assertTrue(is_valid_date('2020-02-29'))
        self.assertTrue(is_valid_date('2020-12-31'))

    def test_is_valid_operation_type(self):
        self.assertFalse(is_valid_operation_type(None))
        self.assertFalse(is_valid_operation_type(''))
        self.assertFalse(is_valid_operation_type('crap'))
        self.assertFalse(is_valid_operation_type(0))
        self.assertTrue(is_valid_operation_type(cash_in))
        self.assertTrue(is_valid_operation_type(cash_out))

    def test_is_valid_operation_amount(self):
        self.assertFalse(is_valid_operation_amount(None))
        self.assertFalse(is_valid_operation_amount(-1))
        self.assertFalse(is_valid_operation_amount(0))
        self.assertTrue(is_valid_operation_amount(0, allow_zero_amount=True))
        self.assertTrue(is_valid_operation_amount(1))
        self.assertTrue(is_valid_operation_amount(Decimal(1)))
        self.assertTrue(is_valid_operation_amount(1.2))
        self.assertTrue(is_valid_operation_amount(12345678901234567890.12))

    def test_is_valid_currency(self):
        self.assertFalse(is_valid_currency_token(None))
        self.assertFalse(is_valid_currency_token(''))
        self.assertFalse(is_valid_currency_token('crap'))
        self.assertFalse(is_valid_currency_token(0))
        self.assertTrue(is_valid_currency_token(eur))
        self.assertTrue(is_valid_currency_token(usd))
        self.assertTrue(is_valid_currency_token(jpy))

    def test_to_decimal(self):
        self.assertRaises(InvalidOperation, lambda: to_decimal(None))
        self.assertRaises(InvalidOperation, lambda: to_decimal(''))
        self.assertEqual(to_decimal('0'), 0)
        self.assertEqual(to_decimal('1'), 1)
        self.assertEqual(to_decimal('-1.2'), Decimal('-1.2'))


class TestValidationErrors(unittest.TestCase):
    def test_validation_errors_none(self):
        self.assertEqual(ValidationErrors().errors, [])

    def test_validate_id(self):
        self.assertEqual(ValidationErrors().validate_id(1).errors, [])
        self.assertEqual(ValidationErrors().validate_id(0).errors, ['Invalid id: 0'])
        self.assertEqual(ValidationErrors().validate_id(-1).errors, ['Invalid id: -1'])
        self.assertEqual(ValidationErrors().validate_id(1.2).errors, ['Invalid id: 1.2'])
        self.assertEqual(ValidationErrors().validate_id(Decimal('1.2')).errors, ['Invalid id: 1.2'])
        self.assertEqual(ValidationErrors().validate_id(None).errors, ['Invalid id: None'])

    def test_validate_customer_type(self):
        self.assertEqual(ValidationErrors().validate_customer_type(natural).errors, [])
        self.assertEqual(ValidationErrors().validate_customer_type(legal).errors, [])
        self.assertEqual(ValidationErrors().validate_customer_type('').errors, ['Invalid customer_type: '])
        self.assertEqual(ValidationErrors().validate_customer_type(None).errors, ['Invalid customer_type: None'])
        self.assertEqual(ValidationErrors().validate_customer_type(0).errors, ['Invalid customer_type: 0'])

    def test_validate_date(self):
        self.assertEqual(ValidationErrors().validate_date('2020-01-01').errors, [])
        self.assertEqual(ValidationErrors().validate_date('').errors, ['Invalid date: '])
        self.assertEqual(ValidationErrors().validate_date(None).errors, ['Invalid date: None'])
        self.assertEqual(ValidationErrors().validate_date(0).errors, ['Invalid date: 0'])

    def test_validate_operation_type(self):
        self.assertEqual(ValidationErrors(
        ).validate_operation_type(cash_in).errors, [])
        self.assertEqual(ValidationErrors(
        ).validate_operation_type(cash_out).errors, [])
        self.assertEqual(ValidationErrors().validate_operation_type('').errors, ['Invalid operation_type: '])
        self.assertEqual(ValidationErrors().validate_operation_type(None).errors, ['Invalid operation_type: None'])
        self.assertEqual(ValidationErrors().validate_operation_type(0).errors, ['Invalid operation_type: 0'])

    def test_validate_operation_amount(self):
        self.assertEqual(ValidationErrors().validate_operation_amount(1).errors, [])
        self.assertEqual(ValidationErrors().validate_operation_amount(Decimal('1.2')).errors, [])
        self.assertEqual(ValidationErrors().validate_operation_amount(None).errors, ['Invalid operation_amount: None'])
        self.assertEqual(ValidationErrors().validate_operation_amount(0).errors, ['Invalid operation_amount: 0'])
        self.assertEqual(ValidationErrors().validate_operation_amount(0, allow_zero_amount=True).errors, [])
        self.assertEqual(ValidationErrors().validate_operation_amount(-1).errors, ['Invalid operation_amount: -1'])

    def test_validate_currency_token(self):
        self.assertEqual(ValidationErrors().validate_currency_token(eur).errors, [])
        self.assertEqual(ValidationErrors().validate_currency_token(usd).errors, [])
        self.assertEqual(ValidationErrors().validate_currency_token(jpy).errors, [])
        self.assertEqual(ValidationErrors().validate_currency_token(None).errors, ['Invalid currency_token: None'])
        self.assertEqual(ValidationErrors().validate_currency_token('').errors, ['Invalid currency_token: '])
        self.assertEqual(ValidationErrors().validate_currency_token(0).errors, ['Invalid currency_token: 0'])

    def test_validate_customer(self):
        self.assertEqual(ValidationErrors(
        ).validate_customer(PersonFactory.create(1, natural)).errors, [])
        self.assertEqual(ValidationErrors(
        ).validate_customer(PersonFactory.create(1, legal)).errors, [])
        self.assertEqual(ValidationErrors().validate_customer(None).errors, ['Invalid customer: None'])
        self.assertEqual(ValidationErrors().validate_customer('').errors, ['Invalid customer: '])
        self.assertEqual(ValidationErrors().validate_customer('crap').errors, ['Invalid customer: crap'])
        self.assertEqual(ValidationErrors().validate_customer(0).errors, ['Invalid customer: 0'])

    def test_validate_account(self):
        customer = PersonFactory.create(1, legal)
        self.assertEqual(ValidationErrors().validate_account(Account(customer, 1)).errors, [])
        self.assertRaises(Exception, lambda: ValidationErrors().validate_account(Account(customer, 0)))
        self.assertEqual(ValidationErrors().validate_account(None).errors, ['Invalid account: None'])
        self.assertEqual(ValidationErrors().validate_account('').errors, ['Invalid account: '])
        self.assertEqual(ValidationErrors().validate_account('crap').errors, ['Invalid account: crap'])
        self.assertEqual(ValidationErrors().validate_account(0).errors, ['Invalid account: 0'])

    def test_multiple_errors(self):
        self.assertEqual(
            get_validation_errors((
                (ValidationErrors.validate_id, None),
                (ValidationErrors.validate_customer_type, None),
                (ValidationErrors.validate_date, None),
                (ValidationErrors.validate_operation_type, None),
                (ValidationErrors.validate_operation_amount, None),
                (ValidationErrors.validate_currency_token, None),
                (ValidationErrors.validate_customer, None),
                (ValidationErrors.validate_account, None),
            )).errors,
            [
                'Invalid id: None',
                'Invalid customer_type: None',
                'Invalid date: None',
                'Invalid operation_type: None',
                'Invalid operation_amount: None',
                'Invalid currency_token: None',
                'Invalid customer: None',
                'Invalid account: None',
            ],
        )
