import sys
import unittest
from io import StringIO
from unittest.mock import patch

import script

# This is the test content of input.csv:
'''2014-12-31,4,natural,cash_out,1200.00   ,EUR
2015-01-01,4,natural,cash_out,1000.00   ,EUR
2016-01-05,4,natural,cash_out,1000.00   ,EUR
2016-01-05,1,natural,cash_in ,200.00    ,EUR
2016-01-06,2,legal  ,cash_out,300.00    ,EUR
2016-01-06,1,natural,cash_out,30000     ,JPY
2016-01-07,1,natural,cash_out,1000.00   ,EUR
2016-01-07,1,natural,cash_out,100.00    ,USD
2016-01-10,1,natural,cash_out,100.00    ,EUR
2016-01-10,2,legal  ,cash_in ,1000000.00,EUR
2016-01-10,3,natural,cash_out,1000.00   ,EUR
2016-02-15,1,natural,cash_out,300.00    ,EUR
2016-02-19,5,natural,cash_out,3000000   ,JPY
'''

# This is the expected output for input.csv:
expected_output_0 = '0.60\n3.00\n0.00\n0.06\n0.90\n0\n0.70\n0.30\n0.30\n5.00\n0.00\n0.00\n8612\n'

test1 = [
    ['2014-12-31', '4', 'natural', 'cash_out', '1200.00', 'EUR'],
    [1, 2, 3],
    ['2014-12-31', '4', 'natural', 'cash_out', '1200.00', 'EUR'],
]
expected_output_1 = '''0.60
Row [1, 2, 3] --> Errors: ['Invalid number of params: 3']
Row ['2014-12-31', '4', 'natural', 'cash_out', '1200.00', 'EUR'] is valid.
'''


class TestScript(unittest.TestCase):
    def test_main(self):
        with patch.object(sys, 'argv', ['../input.csv']):
            with patch('sys.stdout', new=StringIO()) as fake_out:
                self.assertRaises(RuntimeError, lambda: script.main())
        with patch.object(sys, 'argv', [None, './input.csv']):
            with patch('sys.stdout', new=StringIO()) as fake_out:
                script.main()
                self.assertEqual(str(fake_out.getvalue()), expected_output_0)

    def test_process_rows_test1(self):
        with patch('sys.stdout', new=StringIO()) as fake_out:
            self.assertIsNone(script.process_rows(test1))
            self.assertEqual(str(fake_out.getvalue()), expected_output_1)
