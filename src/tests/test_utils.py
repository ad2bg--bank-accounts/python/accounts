import unittest

from utils import find_object_by_property_values


class Obj:
    def __init__(self, a, b):
        self.a = a
        self.b = b


class TestUtils(unittest.TestCase):
    def setUp(self):
        self.dic1 = {'a': 1, 'b': 2}
        self.dic2 = {'a': 3, 'b': 4}
        self.dic3 = {'a': 5, 'b': 6}
        self.obj1 = Obj(1, 2)
        self.obj2 = Obj(3, 4)
        self.obj3 = Obj(5, 6)

        self.empty_list = []
        self.empty_tuple = tuple()

        self.list_d_1 = [self.dic1, self.dic2, self.dic3]
        self.list_o_1 = [self.obj1, self.obj2, self.obj3]

        self.tuple_d_1 = tuple(self.list_d_1)
        self.tuple_o_1 = tuple(self.list_o_1)

        self.generator_d_1 = lambda: (x for x in self.list_d_1)
        self.generator_o_1 = lambda: (x for x in self.list_o_1)

    def test_find_object_by_property_values_with_empty_collection(self):
        self.assertIsNone(find_object_by_property_values(self.empty_list, a=1))
        self.assertIsNone(find_object_by_property_values(self.empty_tuple, a=1))
        self.assertIsNone(find_object_by_property_values((x for x in []), a=1))

    def test_find_object_by_property_values_with_collection_with_dicts(self):
        self.assertIsNone(find_object_by_property_values(self.list_d_1, a=2))
        self.assertIsNone(find_object_by_property_values(self.tuple_d_1, a=2))
        self.assertIsNone(find_object_by_property_values(self.generator_d_1(), a=2))

        self.assertIsNone(find_object_by_property_values(self.list_d_1, a=None))
        self.assertIsNone(find_object_by_property_values(self.tuple_d_1, a=None))
        self.assertIsNone(find_object_by_property_values(self.generator_d_1(), a=None))

        self.assertIsNone(find_object_by_property_values(self.list_d_1, c=0))
        self.assertIsNone(find_object_by_property_values(self.tuple_d_1, c=0))
        self.assertIsNone(find_object_by_property_values(self.generator_d_1(), c=0))

        self.assertDictEqual(find_object_by_property_values(self.list_d_1, a=3), self.dic2)
        self.assertDictEqual(find_object_by_property_values(self.tuple_d_1, a=3), self.dic2)
        self.assertDictEqual(find_object_by_property_values(self.generator_d_1(), a=3), self.dic2)

        self.assertDictEqual(find_object_by_property_values(self.list_d_1, a=5, b=6), self.dic3)
        self.assertDictEqual(find_object_by_property_values(self.tuple_d_1, a=5, b=6), self.dic3)
        self.assertDictEqual(find_object_by_property_values(self.generator_d_1(), a=5, b=6), self.dic3)

    def test_find_object_by_property_values_with_collection_with_objects(self):
        self.assertIsNone(find_object_by_property_values(self.list_o_1, a=2))
        self.assertIsNone(find_object_by_property_values(self.tuple_o_1, a=2))
        self.assertIsNone(find_object_by_property_values(self.generator_o_1(), a=2))

        self.assertIsNone(find_object_by_property_values(self.list_o_1, a=None))
        self.assertIsNone(find_object_by_property_values(self.tuple_o_1, a=None))
        self.assertIsNone(find_object_by_property_values(self.generator_o_1(), a=None))

        self.assertIsNone(find_object_by_property_values(self.list_o_1, c=0))
        self.assertIsNone(find_object_by_property_values(self.tuple_o_1, c=0))
        self.assertIsNone(find_object_by_property_values(self.generator_o_1(), c=0))

        self.assertEqual(find_object_by_property_values(self.list_o_1, a=3), self.obj2)
        self.assertEqual(find_object_by_property_values(self.tuple_o_1, a=3), self.obj2)
        self.assertEqual(find_object_by_property_values(self.generator_o_1(), a=3), self.obj2)

        self.assertEqual(find_object_by_property_values(self.list_o_1, a=5, b=6), self.obj3)
        self.assertEqual(find_object_by_property_values(self.tuple_o_1, a=5, b=6), self.obj3)
        self.assertEqual(find_object_by_property_values(self.generator_o_1(), a=5, b=6), self.obj3)
