def find_object_by_property_values(collection, **kwargs):
    """
    Get the first object from a given list that matches the given property-value pairs.
    :param collection: enumerable
    :param kwargs: prop=value pairs of the object/dict searched for
    :return: object/dict if found, else None
    """

    object_found = None
    for obj in collection:
        try:
            for k, v in kwargs.items():
                try:
                    value = obj[k] if isinstance(obj, dict) else getattr(obj, k)
                except (AttributeError, KeyError):
                    raise AttributeError
                if value != v:
                    raise AttributeError
        except AttributeError:
            continue
        object_found = obj
    return object_found
