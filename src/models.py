from collections import defaultdict
from datetime import timedelta, datetime
from decimal import Decimal
from math import ceil

from utils import find_object_by_property_values
from validation import ValidationErrors, to_decimal, validate_multiple


class Constants:
    # PERSONS
    PERSON_TYPE_NATURAL_KEY = 0
    PERSON_TYPE_LEGAL_KEY = 1
    PERSON_TYPE_NATURAL = 'natural'
    PERSON_TYPE_LEGAL = 'legal'

    PERSON_TYPES = (
        (PERSON_TYPE_NATURAL_KEY, PERSON_TYPE_NATURAL),
        (PERSON_TYPE_LEGAL_KEY, PERSON_TYPE_LEGAL),
    )
    PERSON_TYPES_DICT = dict(PERSON_TYPES)

    # CURRENCIES
    CURRENCY_EUR_KEY = 0
    CURRENCY_USD_KEY = 1
    CURRENCY_JPY_KEY = 2
    CURRENCY_EUR = 'EUR'
    CURRENCY_USD = 'USD'
    CURRENCY_JPY = 'JPY'
    CURRENCY_EUR_RATE = Decimal('1.0')
    CURRENCY_USD_RATE = Decimal('1.1497')
    CURRENCY_JPY_RATE = Decimal('129.53')

    CURRENCIES = (
        {'key': 0, 'token': CURRENCY_EUR, 'rate': CURRENCY_EUR_RATE, 'rounding': 2},
        {'key': 1, 'token': CURRENCY_USD, 'rate': CURRENCY_USD_RATE, 'rounding': 2},
        {'key': 2, 'token': CURRENCY_JPY, 'rate': CURRENCY_JPY_RATE, 'rounding': 0},
    )
    CURRENCY_TOKENS = {(c['key'], c['token']) for c in CURRENCIES}
    CURRENCY_RATES = {(c['key'], c['rate']) for c in CURRENCIES}
    CURRENCY_TOKEN_RATES = {(c['token'], c['rate']) for c in CURRENCIES}
    CURRENCY_ROUNDING = {(c['key'], c['rounding']) for c in CURRENCIES}
    CURRENCY_TOKEN_ROUNDING = {(c['token'], c['rounding']) for c in CURRENCIES}

    CURRENCY_TOKENS_DICT = dict(CURRENCY_TOKENS)
    CURRENCY_RATES_DICT = dict(CURRENCY_RATES)
    CURRENCY_TOKEN_RATES_DICT = dict(CURRENCY_TOKEN_RATES)
    CURRENCY_ROUNDING_DICT = dict(CURRENCY_ROUNDING)
    CURRENCY_TOKEN_ROUNDING_DICT = dict(CURRENCY_TOKEN_ROUNDING)

    BASE_CURRENCY_KEY = CURRENCY_EUR_KEY
    BASE_CURRENCY_TOKEN = CURRENCY_TOKENS_DICT[BASE_CURRENCY_KEY]
    BASE_CURRENCY_RATE = CURRENCY_RATES_DICT[BASE_CURRENCY_KEY]

    # OPERATIONS
    OPERATION_TYPE_CASH_IN_KEY = 0
    OPERATION_TYPE_CASH_OUT_KEY = 1
    OPERATION_TYPE_CASH_IN = 'cash_in'
    OPERATION_TYPE_CASH_OUT = 'cash_out'

    OPERATION_TYPES = (
        (OPERATION_TYPE_CASH_IN_KEY, OPERATION_TYPE_CASH_IN),
        (OPERATION_TYPE_CASH_OUT_KEY, OPERATION_TYPE_CASH_OUT),
    )
    OPERATION_TYPES_DICT = dict(OPERATION_TYPES)


class PersonFactory:
    @classmethod
    def create(cls, customer_id, customer_type):
        validate_multiple((
            (ValidationErrors.validate_id, customer_id),
            (ValidationErrors.validate_customer_type, customer_type),
        ))
        if customer_type == Constants.PERSON_TYPE_NATURAL:
            return NaturalPerson(customer_id)
        if customer_type == Constants.PERSON_TYPE_LEGAL:
            return LegalPerson(customer_id)


class Storage:
    def __init__(self):
        self.customers = {}

    def customer_get_or_create(self, customer_id, customer_type):
        existing_customer = self.customers.get(customer_id)
        if not existing_customer:
            self.customers[customer_id] = new_customer = PersonFactory.create(customer_id, customer_type)
            return new_customer

        if existing_customer.type != customer_type:
            raise Exception([
                f'Discrepancy: Customer {customer_id} is of type {existing_customer.type}, not {customer_type}.',
            ])

        return existing_customer


class Person:
    def __init__(self, customer_id):
        self.id = customer_id
        self.accounts = {}

    def account_get_or_create(self, **kwargs):
        existing_account = find_object_by_property_values(self.accounts.values(), **kwargs)
        if not existing_account:
            id = len(self.accounts) + 1
            self.accounts[id] = new_account = Account(self, id)
            return new_account
        return existing_account


class NaturalPerson(Person):
    def __init__(self, *args):
        super().__init__(*args)
        self.type = Constants.PERSON_TYPE_NATURAL


class LegalPerson(Person):
    def __init__(self, *args):
        super().__init__(*args)
        self.type = Constants.PERSON_TYPE_LEGAL


class Account:
    def __init__(self, customer, account_id):
        validate_multiple((
            (ValidationErrors.validate_customer, customer),
            (ValidationErrors.validate_id, account_id),
        ))
        self.customer = customer
        self.id = account_id
        self.operations = defaultdict(list)  # {date: [operations]} ; we can have multiple operations on a single date

    def get_cash_out_operations_for_the_current_calendar_week(self, date_obj):
        """Return all cash_out operations for the current calendar week (Mon through Sun)"""
        weekday = date_obj.weekday()  # Monday == 0 ... Sunday == 6
        result = []
        indexed_date = date_obj - timedelta(days=weekday)  # last_sunday
        for _ in range(weekday + 1):
            indexed_date_cash_outs = [o for o in self.operations[indexed_date]
                                      if o.operation_type == Constants.OPERATION_TYPE_CASH_OUT]
            result.extend(indexed_date_cash_outs)
            indexed_date += timedelta(days=1)
        return result


class Currency:
    @classmethod
    def convert_to_base_currency(cls, amount, from_currency_token):
        return cls.convert(amount, from_currency_token, Constants.BASE_CURRENCY_TOKEN)

    @classmethod
    def convert_to_currency(cls, amount, to_currency_token):
        return cls.convert(amount, Constants.BASE_CURRENCY_TOKEN, to_currency_token)

    @classmethod
    def convert(cls, amount, from_currency_token, to_currency_token):
        """ Note that this method performs a conversion without rounding. """
        allow_zero_amount = True
        validate_multiple((
            (ValidationErrors.validate_operation_amount, amount, allow_zero_amount),
            (ValidationErrors.validate_currency_token, from_currency_token),
            (ValidationErrors.validate_currency_token, to_currency_token),
        ))
        return (
                Decimal(amount)
                / Constants.CURRENCY_TOKEN_RATES_DICT[from_currency_token]
                * Constants.CURRENCY_TOKEN_RATES_DICT[to_currency_token]
        )


    @staticmethod
    def round_currency(amount, token):
        token_rounding = Constants.CURRENCY_TOKEN_ROUNDING_DICT[token]
        e = 10 ** token_rounding
        return f'{ceil(amount * e) / e:.{token_rounding}f}'


class Operation:
    def __init__(self, account, operation_date, operation_type, operation_amount, operation_currency):
        validate_multiple((
            (ValidationErrors.validate_account, account),
            (ValidationErrors.validate_date, operation_date),
            (ValidationErrors.validate_operation_type, operation_type),
            (ValidationErrors.validate_operation_amount, operation_amount),
            (ValidationErrors.validate_currency_token, operation_currency),
        ))
        self.operation_date = datetime.strptime(str(operation_date), '%Y-%m-%d').date()
        self.operation_type = operation_type
        self.operation_amount = to_decimal(operation_amount)
        self.operation_currency = operation_currency
        self.account = account
        self.account.operations[self.operation_date].append(self)

    def calculate_fee(self):
        if self.operation_type == Constants.OPERATION_TYPE_CASH_IN:
            return self.calculate_fee_for_cash_in()
        if self.operation_type == Constants.OPERATION_TYPE_CASH_OUT:
            return self.calculate_fee_for_cash_out()

    def calculate_fee_for_cash_out(self):
        if isinstance(self.account.customer, NaturalPerson):
            return self.calculate_fee_for_cash_out_for_natural_person()
        if isinstance(self.account.customer, LegalPerson):
            return self.calculate_fee_for_cash_out_for_legal_person()

    def calculate_fee_for_cash_in(self):
        """ Commission fee - 0.03% from total amount, but no more than 5.00 EUR. """

        percentage = Decimal('0.0003')
        max_fee = Decimal('5'), 'EUR'
        return self.convert_to_this_currency_token_and_round(
            fee_in_base_currency=min(
                Currency.convert_to_base_currency(
                    amount=self.operation_amount * percentage,
                    from_currency_token=self.operation_currency,
                ),
                Currency.convert_to_base_currency(*max_fee),
            ),
        )

    def calculate_fee_for_cash_out_for_natural_person(self):
        """
        Default commission fee - 0.3% from cash out amount.
        1000.00 EUR per week (from monday to sunday) is free of charge.
        If total cash out amount is exceeded - commission is calculated only from exceeded amount
        (that is, for 1000.00 EUR there is still no commission fee).
        This discount is applied only for first 3 cash out operations per week for each user -
        for forth and other operations commission is calculated by default rules (0.3%) -
        rule about 1000 EUR is applied only for first three cash out operations.
        """

        percentage = Decimal('0.003')
        free_amount_per_calendar_week = Decimal('1000.00'), 'EUR'
        eligible_number_of_operations_per_week = 3

        taxable_amount_in_base_currency = Currency.convert_to_base_currency(
            amount=self.operation_amount,
            from_currency_token=self.operation_currency,
        )

        this_week_cash_outs = self.account.get_cash_out_operations_for_the_current_calendar_week(self.operation_date)
        if len(this_week_cash_outs) <= eligible_number_of_operations_per_week:
            free_weekly_amount_in_base_currency = Currency.convert_to_base_currency(*free_amount_per_calendar_week)
            total_weekly_amount_in_base_currency = sum(
                Currency.convert_to_base_currency(
                    amount=o.operation_amount,
                    from_currency_token=o.operation_currency,
                )
                for o in this_week_cash_outs
            )  # this includes self.operation_amount

            taxable_weekly_amount_in_base_currency = max(
                0, (total_weekly_amount_in_base_currency - free_weekly_amount_in_base_currency))

            if taxable_amount_in_base_currency > taxable_weekly_amount_in_base_currency:
                taxable_amount_in_base_currency = taxable_weekly_amount_in_base_currency

        return self.convert_to_this_currency_token_and_round(
            fee_in_base_currency=taxable_amount_in_base_currency * percentage,
        )

    def calculate_fee_for_cash_out_for_legal_person(self):
        """ Commission fee - 0.3% from amount, but not less than 0.50 EUR for operation. """

        percentage = Decimal('0.003')
        min_fee = Decimal('0.5'), 'EUR'
        return self.convert_to_this_currency_token_and_round(
            fee_in_base_currency=max(
                Currency.convert_to_base_currency(
                    amount=self.operation_amount * percentage,
                    from_currency_token=self.operation_currency,
                ),
                Currency.convert_to_base_currency(*min_fee),
            ),
        )

    def convert_to_this_currency_token_and_round(self, fee_in_base_currency):
        fee_in_operation_currency = Currency.convert_to_currency(
            amount=fee_in_base_currency,
            to_currency_token=self.operation_currency,
        )
        return Currency.round_currency(
            amount=fee_in_operation_currency,
            token=self.operation_currency,
        )
