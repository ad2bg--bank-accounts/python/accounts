import csv
import sys

from models import Storage, Operation

expected_number_of_params_per_row = 6
is_file_corrupt = False


def main():
    if len(sys.argv) != 2:
        raise RuntimeError('Invalid number of arguments!')
    with open(sys.argv[1]) as csv_file:
    # with open('../input.csv') as csv_file:  # comment-out the above lines & uncomment this one to run/debug in PyCharm
        process_rows(csv.reader(csv_file))


def process_rows(rows):
    storage = Storage()
    for row in rows:
        process_row(row, storage)


def process_row(row, storage):
    global is_file_corrupt
    params = [str(p).strip() for p in row]  # strip spaces
    row_errors = None
    try:
        if len(params) != expected_number_of_params_per_row:
            raise Exception([f'Invalid number of params: {len(params)}'])
        (operation_date, customer_id, user_type, operation_type, operation_amount, operation_currency) = params

        customer = storage.customer_get_or_create(customer_id, user_type)

        # Refactor the following line accordingly
        # in case the business logic changes to support multiple accounts per customer
        account = customer.account_get_or_create(id=1)

        operation = Operation(account, operation_date, operation_type, operation_amount, operation_currency)
        if not is_file_corrupt:
            print(operation.calculate_fee())
            return
    except Exception as e:
        row_errors = e
        is_file_corrupt = True
    if row_errors:
        print(f'Row {row} --> Errors: {row_errors}')
    else:
        print(f'Row {row} is valid.')


(__name__ == '__main__') and main()
