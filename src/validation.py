import datetime
import decimal
from decimal import Decimal


def is_valid_id(id):
    return str(id).isdigit() and id not in [0, '0']  # isdigit() only returns True for positive integers and 0


def is_valid_customer_type(customer_type):
    from models import Constants
    return customer_type in dict(Constants.PERSON_TYPES_DICT).values()


def is_valid_date(date_text):
    try:
        datetime.datetime.strptime(str(date_text), '%Y-%m-%d')
        return True
    except ValueError:
        return False


def is_valid_operation_type(operation_type):
    from models import Constants
    return operation_type in Constants.OPERATION_TYPES_DICT.values()


def is_valid_operation_amount(operation_amount, allow_zero_amount=False):
    try:
        x = to_decimal(operation_amount)
    except decimal.InvalidOperation:
        return False
    return x >= 0 if allow_zero_amount else x > 0


def is_valid_currency_token(currency_token):
    from models import Constants
    return currency_token in Constants.CURRENCY_TOKENS_DICT.values()


def to_decimal(x):
    return Decimal(str(x).replace(',', '.'))  # depending on locale the comma may be a valid decimal separator


class ValidationErrors:
    def __init__(self):
        self.errors = []

    def validate_id(self, id):
        if not is_valid_id(id):
            self.errors.append(f'Invalid id: {id}')
        return self

    def validate_customer_type(self, customer_type):
        if not is_valid_customer_type(customer_type):
            self.errors.append(f'Invalid customer_type: {customer_type}')
        return self

    def validate_date(self, date_text):
        if not is_valid_date(date_text):
            self.errors.append(f'Invalid date: {date_text}')
        return self

    def validate_operation_type(self, operation_type):
        if not is_valid_operation_type(operation_type):
            self.errors.append(f'Invalid operation_type: {operation_type}')
        return self

    def validate_operation_amount(self, operation_amount, allow_zero_amount=False):
        if not is_valid_operation_amount(operation_amount, allow_zero_amount=allow_zero_amount):
            self.errors.append(f'Invalid operation_amount: {operation_amount}')
        return self

    def validate_currency_token(self, currency_token):
        if not is_valid_currency_token(currency_token):
            self.errors.append(f'Invalid currency_token: {currency_token}')
        return self

    def validate_customer(self, customer):
        from models import Person
        if not isinstance(customer, Person):
            self.errors.append(f'Invalid customer: {customer}')
        return self

    def validate_account(self, account):
        from models import Account
        if not isinstance(account, Account):
            self.errors.append(f'Invalid account: {account}')
        return self


def validate_multiple(validations):
    validation_errors = get_validation_errors(validations)
    if validation_errors.errors:
        raise Exception(validation_errors.errors)


def get_validation_errors(validations):
    validation_errors = ValidationErrors()
    for (func, *args) in validations:
        validation_errors = getattr(validation_errors, func.__name__)(*args)
    return validation_errors
